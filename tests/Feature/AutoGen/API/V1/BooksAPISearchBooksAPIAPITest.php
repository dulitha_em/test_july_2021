<?php

namespace Tests\Feature\AutoGen\API\V1;

use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class BooksAPISearchBooksAPIAPITest extends APIBaseTestCase
{
    use DatabaseTransactions;

    /**
     *
     *
     *
     * @return  void
     */
    public function test_api_booksapi_get_search_booksapi()
    {
        $data = $cookies = $files = $headers = $server = [];
        $faker = \Faker\Factory::create('en_AU');
        $content = null;

        // header params
        $headers['Accept'] = 'application/json';
        $headers['x-access-token'] = $this->getAccessToken();
        $headers['x-api-key'] = $this->getApiKey();


        $response = $this->get('/api/v1/books', $headers);

        $this->saveResponse($response->getContent(), 'booksapi_get_search_booksapi', $response->getStatusCode());

        $response->assertStatus(200);

        $response->assertJsonStructure([
            'payload',
            'message',
            'result'
        ]);
    }

    /**
     *
     *
     * @test
     * @return  void
     */
    public function test_api_booksapi_get_search_booksapi_search()
    {
        $data = $cookies = $files = $headers = $server = [];
        $faker = \Faker\Factory::create('en_AU');
        $content = null;

        // header params
        $headers['Accept'] = 'application/json';
        $headers['x-access-token'] = $this->getAccessToken();
        $headers['x-api-key'] = $this->getApiKey();


        $response = $this->get('/api/v1/books?q=abc', $headers);

        $this->saveResponse($response->getContent(), 'booksapi_get_search_booksapi', $response->getStatusCode());

        $response->assertStatus(200);

        $response->assertJsonStructure([
            'payload',
            'message',
            'result'
        ]);
    }

    /**
     *
     *
     * @test
     * @return  void
     */
    public function test_api_booksapi_get_search_booksapi_order()
    {
        $data = $cookies = $files = $headers = $server = [];
        $faker = \Faker\Factory::create('en_AU');
        $content = null;

        // header params
        $headers['Accept'] = 'application/json';
        $headers['x-access-token'] = $this->getAccessToken();
        $headers['x-api-key'] = $this->getApiKey();


        $response = $this->get('/api/v1/books?orderByYear=0', $headers);
        $this->saveResponse($response->getContent(), 'booksapi_get_search_booksapi', $response->getStatusCode());
        $response->assertStatus(200);
        $response->assertJsonStructure([
            'payload',
            'message',
            'result'
        ]);

        $response = $this->get('/api/v1/books?orderByYear=1', $headers);
        $this->saveResponse($response->getContent(), 'booksapi_get_search_booksapi', $response->getStatusCode());
        $response->assertStatus(200);
        $response->assertJsonStructure([
            'payload',
            'message',
            'result'
        ]);
    }
}
