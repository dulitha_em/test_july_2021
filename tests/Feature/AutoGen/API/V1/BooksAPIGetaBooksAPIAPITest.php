<?php

namespace Tests\Feature\AutoGen\API\V1;

use App\Entities\Books\Book;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class BooksAPIGetaBooksAPIAPITest extends APIBaseTestCase
{
    use DatabaseTransactions;

    /**
     *
     *
     *
     * @return  void
     */
    public function test_api_booksapi_get_get_a_booksapi()
    {
        $data = $cookies = $files = $headers = $server = [];
        $faker = \Faker\Factory::create('en_AU');
        $content = null;

        // header params
        $headers['Accept'] = 'application/json';
        $headers['x-access-token'] = $this->getAccessToken();
        $headers['x-api-key'] = $this->getApiKey();


        $response = $this->get('/api/v1/books/1', $headers);

        $this->saveResponse($response->getContent(), 'booksapi_get_get_a_booksapi', $response->getStatusCode());

        $response->assertStatus(200);
    }

    /**
     *
     *
     *
     * @return  void
     */
    public function test_api_booksapi_get_get_a_booksapi_fail()
    {
        $data = $cookies = $files = $headers = $server = [];
        $faker = \Faker\Factory::create('en_AU');
        $content = null;

        // header params
        $headers['Accept'] = 'application/json';
        $headers['x-access-token'] = $this->getAccessToken();
        $headers['x-api-key'] = $this->getApiKey();

        $bookId = 1000;
        $book = Book::find($bookId);
        ($book) ? $book->delete() : '';

        $response = $this->get('/api/v1/books/$bookId', $headers);

        $this->saveResponse($response->getContent(), 'booksapi_get_get_a_booksapi', $response->getStatusCode());

        $response->assertStatus(422);
    }
}
