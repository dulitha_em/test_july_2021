<?php

namespace App\Entities\Books;

use App\Entities\BaseRepository;

class BooksRepository extends BaseRepository
{

	public function __construct(Book $model)
	{
		parent::__construct($model);
	}

}
