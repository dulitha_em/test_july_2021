<?php

namespace App\Http\Controllers\API\V1;

use App\Entities\Books\Book;
use App\Entities\Books\BooksRepository;
use App\Http\Controllers\API\V1\APIBaseController;
use EMedia\Api\Docs\APICall;
use EMedia\Api\Docs\Param;
use Illuminate\Http\Request;

class BooksAPIController extends APIBaseController
{

	protected $repo;

	public function __construct(BooksRepository $repo)
	{
		$this->repo = $repo;
	}

	protected function index(Request $request)
	{
		document(function () {
                	return (new APICall())
                	    ->setParams([
                	        'q|Search query',
                	        'page|Page number',
                            (new Param('orderByYear', Param::TYPE_BOOLEAN , 'Order by year (0 or 1)', Param::LOCATION_QUERY)),
                        ])
                        ->setSuccessPaginatedObject(Book::class);
                });

        $filter = $this->repo->newSearchFilter();
		if($request->input('orderByYear', false)) {
            $filter->setQuery(Book::orderBy('year'));
        }

		$items = $this->repo->search($filter);

		return response()->apiSuccess($items);
	}

	public function show($id) {
        document(function () {
            return (new APICall())
                ->setParams([
                    (new Param('book', Param::TYPE_INT, 'ID of the book', Param::LOCATION_PATH)),
                ])
                ->setSuccessObject(Book::class);
        });

        $book = Book::find($id);

        if($book) {
            return response()->apiSuccess($book);
        }
        else {
            return response()->apiError('Book not found!');
        }
    }

}
